namespace RPGHtesting;
using Xunit; //For the testing
using RPGH.Characters;
using RPGH.Characters.Heroes;


public class HeroTest
{
    /// <summary>
    /// Hello Saju! Because of a Git pull incident in Git bash, I lost two days of progress on tuesday.
    /// So its because of that I have no time to finish the testing, If I had one more day I think I could manage.
    /// Sadly that's not the case. I was watching a youtube guide to push for the second time, 
    /// and when I did I got an error saying that I had to pull the old version before I could push the 
    /// new one so I did. ANd then it overwrite my new files. Ive had some long days trying to recover from my 
    /// mistake.
    /// </summary>


    [Fact]
    public void Mage_Should_Have_Correct_Name_Level_Attributes_When_Created()
    {
        //Arrange - section of a unit test method initializes objects and sets the value of the data to method
        string expectedName = "Jarand";
        int expectedLevel = 1;
        HeroAttributes expectedAttributes = new HeroAttributes()
        {
            Strength = 1,
            Dexterity = 1,
            Intelligence = 8,
        };

        //Act - section invokes the method under test with the given parameters
        var mage = new Mage(expectedName);


        //Assert - Verifies that the action behaves as expected
        Assert.Equal(expectedName, mage.Name);
        Assert.Equal(expectedLevel, mage.level);
        Assert.Equal(expectedAttributes.ToString(), mage.defaultAttributes.ToString());
        //Assert.Equal(expectedLevel, mage.levelUp(10));

    }
    
    [Fact]

    public void Ranger_Should_Have_Correct_Name_Level_Attributes_When_Created()
    {
        //Arrange - section of a unit test method initializes objects and sets the value of the data to method
        string expectedName = "Jarand";

        int expectedLevel = 1;
        HeroAttributes expectedAttributes = new HeroAttributes()
        {
            Strength = 1,
            Dexterity = 7,
            Intelligence = 1,
        };

        //Act - section invokes the method under test with the given parameters
        var ranger = new Ranger(expectedName);

        //Assert - Verifies that the action behaves as expected
        Assert.Equal(expectedName, ranger.Name);
        Assert.Equal(expectedLevel, ranger.level);
        Assert.Equal(expectedAttributes.ToString(), ranger.defaultAttributes.ToString());


    }
    
}



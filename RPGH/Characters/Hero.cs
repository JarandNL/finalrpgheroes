﻿using RPGH.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;


namespace RPGH.Characters
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int level { get; set; } = 1;

        //private HeroAttributes defaultAttribute;

        public HeroAttributes defaultAttributes { get; set; }

        //public HeroAttributes levelAttributes { get; }

        public Dictionary<ItemSlot, Items.Items> Equipment { get; }
        



        //Creating a constructor for the heroes and its attributes.
        public Hero(string name, int strength, int dexterity, int intelligence)
        {
            Name = name;
            level = 1;
            Equipment = new Dictionary<ItemSlot, Items.Items>()
            {
                {ItemSlot.weapon_Slot, null},
                {ItemSlot.head_Slot, null},
                {ItemSlot.body_Slot, null},
                {ItemSlot.legs_Slot, null}
            };
            defaultAttributes = new HeroAttributes() { Strength = strength, Dexterity = dexterity,
                                                        Intelligence = intelligence };

        }
        

        public abstract void levelUp(int currentLevel);
        public abstract string equipWeapon(Weapon weapon);
        public abstract string equipArmor(Armor armor);

        public double weaponDamage { get; set; }

        public abstract double CalculateDamage();

        public HeroAttributes ArmorAttributes()
        {
            HeroAttributes armorAttributes = new() { Strength = 0, Dexterity = 0, Intelligence = 0 };
            bool ifHeadArmor = Equipment.TryGetValue(ItemSlot.head_Slot, out Items.Items head_Slot);
            bool ifBodyArmor = Equipment.TryGetValue(ItemSlot.body_Slot, out Items.Items body_Slot);
            bool ifLegsArmor = Equipment.TryGetValue(ItemSlot.legs_Slot, out Items.Items legs_Slot);
            if (head_Slot != null)
            {
                if (ifHeadArmor is Armor)
                {
                    Armor head = (Armor)head_Slot;
                    armorAttributes += new HeroAttributes()
                    {
                        Strength = head.Attributes.Strength,
                        Dexterity = head.Attributes.Dexterity,
                        Intelligence = head.Attributes.Intelligence
                    };
                }
            }
            if (body_Slot != null)
            {
                if (ifBodyArmor is Armor)
                {
                    Armor body = (Armor)body_Slot;
                    armorAttributes += new HeroAttributes()
                    {
                        Strength = body.Attributes.Strength,
                        Dexterity = body.Attributes.Dexterity,
                        Intelligence = body.Attributes.Intelligence
                    };
                }
            }
            if (legs_Slot != null)
            {
                if (ifLegsArmor is Armor)
                {
                    Armor legs = (Armor)legs_Slot;
                    armorAttributes += new HeroAttributes()
                    {
                        Strength = legs.Attributes.Strength,
                        Dexterity = legs.Attributes.Dexterity,
                        Intelligence = legs.Attributes.Intelligence
                    };
                }

            }
            return defaultAttributes += armorAttributes;
        }

        public abstract HeroAttributes TotalAttributes();

        public abstract string HeroDisplay(HeroAttributes TotalAttributes);


    }
}

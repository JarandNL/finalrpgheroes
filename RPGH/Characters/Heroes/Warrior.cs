﻿using RPGH.Exceptions;
using RPGH.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPGH.Characters.Heroes
{
    public class Warrior : Hero
    {

        //Creating a constructor with passing just a name and its attributes
        public Warrior(string name) : base(name, 5, 2, 1)
        {
        }

        //Creating a levelup method where you increase lvl and attributes when leveling up
        public override void levelUp(int currentLevel)
        {
            if (currentLevel < 1) throw new ArgumentException();

            HeroAttributes levelAttributes = new()
            {
                Strength = 3 * currentLevel,
                Dexterity = 2 * currentLevel,
                Intelligence = 1 * currentLevel
            };
            defaultAttributes += levelAttributes;

            level += 1 * currentLevel;
        }
        //Creating a method for equipping Weapon
        public override string equipWeapon(Weapon weapon)
        {

            if (weapon.requiredItemLevel > level)
                throw new InvalidWeaponException($"Not high enough level." +
                    $"Required level to equip this weapon is: {weapon.requiredItemLevel}");

            if (weapon.WeaponType != Weapon.WeaponTypes.Axe && weapon.WeaponType != Weapon.WeaponTypes.Hammer
                                        && weapon.WeaponType != Weapon.WeaponTypes.Sword)
                throw new InvalidWeaponException($"Not the right weapon." +
                    $"your character can't equip a {weapon.WeaponType} . Try equipping an axe, hammer or a sword.");

            else
                Equipment[weapon.ItemSlot] = weapon;
            weaponDamage += weapon.WeaponDamage;
            return "New weapon Equipped!";
        }
        //Creating a method for equipping Armor
        public override string equipArmor(Armor armor)
        {
            HeroAttributes heroAttributes = new HeroAttributes();

            //Checking if the required item level is higher then your level
            if (armor.requiredItemLevel > level)
                throw new InvalidArmorException($"Not high enough level." +
                    $"Required level to equip this armor is: {armor.requiredItemLevel}");

            //Checking if the armor is incorrect
            if (armor.ArmorType != Armor.ArmorTypes.Mail && armor.ArmorType != Armor.ArmorTypes.Plate)
                throw new InvalidArmorException($"Not the right armor." +
                    $"your character can't equip {armor.ArmorType}. Try equipping some Mail og Plate.");
            //If neither of the previous checks is correct, the armor will be equipped,  along with its attributes.
            else
                Equipment[armor.ItemSlot] = armor;
                heroAttributes += armor.Attributes;
            return "New armor Equipped!";
        }


        /// <summary>
        /// Create a method to calculate damage without storing
        /// I multiply weapon damage with level
        /// I add the characters main attribute to scale with damage (In this case Strength)
        /// </summary>
        /// <returns>total damage (weapondamage + level scaled damage</returns>
        public override double CalculateDamage()
        {
            double damage = weaponDamage * level;
            HeroAttributes totalAttributes = TotalAttributes();
            damage += totalAttributes.Strength / 100;
            return damage;
        }


        /// <summary>
        /// Create a method to calculate attributes without storing
        /// Here I first check if Equipment is not null - this is good practise
        /// Then I loop through Equipment to see if there are any armors equipped, and if it is, it will add its attributes
        /// </summary>
        /// <returns>attributes + armor attributes</returns>
        public override HeroAttributes TotalAttributes()
        {
            HeroAttributes totalAttributes = new HeroAttributes();
            totalAttributes += defaultAttributes;
            totalAttributes += ArmorAttributes();
            if (Equipment != null)
            {
                foreach (var item in Equipment)
                {
                    if (item.Value is Armor)
                    {
                        totalAttributes += ((Armor)item.Value).Attributes;
                    }
                }
            }
            return totalAttributes;
        }

        /// <summary>
        /// Create a method that displays all detail of the hero. Its total attributes including armor and levels
        /// </summary>
        /// <param name="TotalAttributes"></param>
        /// <returns></returns>
        public override string HeroDisplay(HeroAttributes TotalAttributes)
        {
            return $"Name: {Name}\nLevel: {level}\nAttributes: Strength {TotalAttributes.Strength} " +
                    $"Dexterity {TotalAttributes.Dexterity} Intelligence {TotalAttributes.Intelligence}\n" +
                    $"Equipped weapon: {Equipment[ItemSlot.weapon_Slot]}\n" +
                    $"Equipped armor: {Equipment[ItemSlot.head_Slot]}\n" +
                    $"Equipped armor: {Equipment[ItemSlot.body_Slot]}\n" +
                    $"Equipped armor: {Equipment[ItemSlot.legs_Slot]}\n" +
                    $"Damage: {CalculateDamage()}";
        }
    }
}




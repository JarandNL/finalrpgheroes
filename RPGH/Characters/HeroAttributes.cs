﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGH.Characters
{
    public class HeroAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }


        //Creating a method to add two instances together and return the increased value by a specified amount
        public static HeroAttributes operator + (HeroAttributes attributes, HeroAttributes attributes2)
        {
            HeroAttributes heroAttributes = attributes;
            heroAttributes.Strength += attributes2.Strength;
            heroAttributes.Dexterity += attributes2.Dexterity;
            heroAttributes.Intelligence += attributes2.Intelligence;
            return heroAttributes;
        }
        public override string ToString()
        {
            return $"Your heroe's attributes are: {{ Strength: {Strength}, " +
                $"Dexterity: {Dexterity}, " +
                $"Intelligence: {Intelligence} }}";
        }

    }
}

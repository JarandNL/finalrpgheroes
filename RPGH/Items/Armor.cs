﻿using RPGH.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace RPGH.Items
{
    public class Armor : Items
    {
        public enum ArmorTypes
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
        public ArmorTypes ArmorType { get; set; }

        private HeroAttributes armorAttributes;
        public HeroAttributes Attributes { get { return armorAttributes; } set { armorAttributes = value; } }

        //Creating a constructor with the parameters that I need to create an Armor.
        public Armor(string itemName, int requiredItemLevel, ItemSlot itemSlot, HeroAttributes armorAttributes) : base(itemName, requiredItemLevel, itemSlot)
        {
            this.armorAttributes = armorAttributes;
        }


        public override string ItemInfo()
        {
            return $"Armor type is: {ArmorType}";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGH.Items
{
    public enum ItemSlot
    {
        weapon_Slot,
        head_Slot,
        body_Slot,
        legs_Slot
        
    }
    public abstract class Items
    {
        public string ItemName { get; set; }
        public int requiredItemLevel { get; set; }
        public ItemSlot ItemSlot { get; set; }

        public Items(string itemName, int requiredItemLevel, ItemSlot itemSlot)
        {
            ItemName = itemName;
            this.requiredItemLevel = requiredItemLevel;
            this.ItemSlot = itemSlot;
        }

        public abstract string ItemInfo();


    }
}

﻿using RPGH.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGH.Items
{

    public class Weapon : Items
    {
        //List of weapontypes
        public enum WeaponTypes
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }
        
        //Creating a constructor with the parameters that I need to create a weapon.
        public Weapon(string itemName, int requiredItemLevel, ItemSlot itemSlot, WeaponTypes weaponType, double weaponDamage) : base(itemName, requiredItemLevel, itemSlot)
        {
            WeaponType = weaponType;
            WeaponDamage = weaponDamage;
        }

        public WeaponTypes WeaponType { get; set; }

        //Setting default weapon damage in case nothing is specified on creation.
        public double WeaponDamage = 1;



        public override string ItemInfo()
        {
            return $"Weapon type is: {WeaponType}";
        }


    }
}

# RPG Heroes

## Introduction

This is a console game written in c#.


## How does the game work

This is a game that consists of four types of heroes; Mage, Ranger, Rogue & Warrior.
These heroes have different attributes; Strength, Dexterity, Intelligence.
The different type of heroes scale their damage with one of the following attribute depending on type, 
ex: Warrior's strength scales with Strength, Mage with Intelligence etc.
Heroes can only equip specific items (armor and weapons) based on their hero type.
ex: Ranger can only equip bow as weapon and leather and mail as armor.
You can level up your hero to increase your stats/attributes. You can equip weapon and armor to increase both your
attributes and you damage. 


# Heroes, stats & equipment

## Mage
A mage starts with following attributes: Strength = 1 | Dexterity = 1 | Intelligence = 8
A mage grants the following attributes per level up: Strength = 1 | Dexterity = 1 | Intelligence = 5
Mage's damage scales with 1% of her scaling attribute (Intelligence). In lvl 1 this is 8/100 = 0.8damage

Mage's equipable weapons are: Staff & Wand
Mage's equipable armor are: Cloth

## Ranger
A ranger starts with following attributes: Strength = 1 | Dexterity = 7 | Intelligence = 1
A ranger grants the following attributes per level up: Strength = 1 | Dexterity = 5 | Intelligence = 1
Ranger's damage scales with 1% of her scaling attribute (Dexterity) In lvl 1 this is 7/100 = 0.7damage

Ranger's equipable weapons are: Bow
Ranger's equipable armor are: Leather & Mail

## Rogue
A rogue starts with following attributes: Strength = 2 | Dexterity = 6 | Intelligence = 1
A rogue grants the following attributes per level up: Strength = 1 | Dexterity = 4 | Intelligence = 1
Rogue's damage scales with 1% of her scaling attribute (Dexterity) In lvl 1 this is 6/100 = 0.6damage

Rogue's equipable weapons are: Dagger & Sword
Rogue's equipable armor are: Leather & Mail


## Warrior
A Warrior starts with following attributes: Strength = 5 | Dexterity = 2 | Intelligence = 1
A Warrior grants the following attributes per level up: Strength = 3 | Dexterity = 2 | Intelligence = 1
Warrior's damage scales with 1% of her scaling attribute (Strength) In lvl 1 this is 5/100 = 0.5damage

Warrior's equipable weapons are: Axe, Hammer & Sword
Warrior's equipable armor are: Mail & Plate

